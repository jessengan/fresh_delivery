# 生鲜配送平台

## 介绍
本平台为商家提供基于地理位置定位的多商户的生鲜配送服务。为用户端提供可定制配送服务的APP及微信小程序的应用。为送货小哥提供送货端APP服务。

## 平台功能介绍
### 平台角色
1. 系统超级管理员：整个系统的全局管理、查看整个系统的所有数据，只能登录网页后台
2. 城市（代理）管理员：对整个代理城市辖区的具备管理权限，能够管理辖区范围内的所有配送站、配送员、商品、订单、及对应数据报表等数据，只能登录网页后台。
3. 配送站站长：对当前配送站的配送数据进行管理，能够分配配送站的的订单给配送员，查看管辖配送站的订单数据，只能登录网页后台。
4. 配送员：接收分配的配送订单，一个配送员可以服务1个或者多个配送站，只能登录配送员的专属APP。
5. 普通用户（消费者）：通过在用户端APP下单消费、支付订单、查看订单配送进度、对订单进行评价等。

## 技术相关
#### 软件架构
主要分为biz、comm、api、web三个部分：

1. biz：用于处理数据库及相关业务的代码。
2. comm：用于提供公共类及方法，保存静态类。
3. api：为APP和小程序提供接口。
4. web：为整个系统的业务管理后台

#### 采用的开发技术有：

0. 整个系统采用JFinal3.8 框架开发
1. 数据库：MySql5.7（支持JSON和GEO计算），不要使用MariaDb（不支持JSON和GEO计算）。
2. 连接池：druid
3. 缓存：ehcache（支持组播的缓存共享功能）
4. Excel：POI用于数据导出业务
5. HTML网页消息推送：pushlet（基于servlet的技术，由Java代码向前端html推送消息）
6. 短信服务：阿里云短信
7. 对象存储：阿里云OSS
8. 地图定位：百度地图（手机SDK 和 网页地图 ）
9. 网页消息：采用Pushlet（基于Java Servlet开发的推送组件，通过Java触发向网页发送消息）
10. 支付方式：在APP中支持支付宝和微信支付，支持微信小程序APP的微信支付。
11. 第三方SDK：支持微博、微信、QQ的第三方登录及注册服务。
12. 定时任务：采用quartz实现的定时任务
13. APP消息推送：采用极光消息推送SDK，用来在后台有优惠券、新订单、以及订单状态时向APP推送通知消息。


#### 安装准备清单
1.  准备部署的服务器（Windows2008+或者Centos6.5+操作系统）
2.  安装MySQL5.7+以上的数据库服务器。
3.  申请阿里云的短信服务
4.  申请阿里云OSS服务
5.  百度地图开发的 APPID 和 SecretKey
6.  开通支付宝商户账户，提供APPID、SecretKEY、安全证书。
7.  开通微信支付商户账户，提供APPID 、 SecretKEY、安装证书。
9.  提供极光推送SDK的APPID 、SecretKey
10.  申请提供微博开放平台SDK、微信开放平台SDK、QQ开放平台SDK。

#### 安装教程

推荐部署结构图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0506/172455_271b7917_130557.png "TIM截图20200506171508.png")

1.  在服务器中部署nginx和MySql5.7+的数据库（必须要5.7以上版本，支持JSON查询和GEO运算），不要采用MariaDb。
2.  在服务器中安装两个tomcat分别将端口设置成为8080 和 8081，8080的tomcat部署API应用，8081的tomcat部署Web的应用。
3.  将API和WEB的war包分别拷贝到8080和8081的tomcat webapps目录下。
4.  在Nginx中配置8080和8081两个tomcat的代理，对外的所有方位都通过Nginx进行代理中转，方便为以后系统升级及负载均衡准备。
5.  在API和WEB中的ehcache.xml中配置开通基于服务器内网组播的缓存共享服务，用于API和WEB两个工程应用之间缓存共享互动，在WEB端修改配置后通过缓存共享及时更新到API中，不需要重新启动tomcat不影响业务。

#### APP部分效果图

![登录界面](https://images.gitee.com/uploads/images/2021/0206/172024_22526daa_130557.png "登录.png")
![个人中心](https://images.gitee.com/uploads/images/2021/0206/172047_b086494b_130557.png "个人中心.png")
![APP首页](https://images.gitee.com/uploads/images/2021/0206/172106_ae596e0e_130557.png "首页.png")
![分类](https://images.gitee.com/uploads/images/2021/0206/172143_5e54a2a9_130557.png "分类.png")
![商品详情](https://images.gitee.com/uploads/images/2021/0206/172224_5d10700e_130557.png "商品详情 秒杀.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0206/172300_d5735fbe_130557.png "商品详情 普通.png")

#### WEB后台管理界面



#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
